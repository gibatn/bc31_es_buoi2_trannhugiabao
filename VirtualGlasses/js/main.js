import { dataGlasses } from "./model/glassModel.js";
import { glassController } from "./controller/glassController.js";
glassController.renderHTML(dataGlasses);
let removeArr = null;
let changeGlasses = (x) => {
  for (let i in dataGlasses) {
    if (dataGlasses[i].id === x) {
      document.querySelector(".vglasses__info").style.display = "block";

      testGlasses(dataGlasses[i].virtualImg);
      renderlide(dataGlasses[i]);
      removeArr = i;
    }
  }
};
window.changeGlasses = changeGlasses;

let renderlide = (x) => {
  let htmlSlide = "";

  htmlSlide += `<p><span class = "text-uppercase font-weight-bold display-5">${x.name} - ${x.brand}</span> <span class="font-weight-bold display-5"> (${x.color})</span></p>  <button class="btn btn-danger">${x.price}</button> <span class="text-success">Stocking</span>
    <p>${x.description}</p>`;

  document.getElementById("hello").innerHTML = htmlSlide;
};

let testGlasses = (x) => {
  let test = "";

  test = `<img src=" ${x}" alt="">`;

  document.getElementById("avatar").innerHTML = test;
};

let removeGlasses = (value) => {
  if (value) {
    if (removeArr < 8) {
      removeArr++;
      testGlasses(dataGlasses[removeArr].virtualImg);
      renderlide(dataGlasses[removeArr]);
    } else {
      removeArr = 0;
      testGlasses(dataGlasses[removeArr].virtualImg);
      renderlide(dataGlasses[removeArr]);
    }
  } else {
    if (removeArr <= 8 && removeArr > 0) {
      removeArr--;
      testGlasses(dataGlasses[removeArr].virtualImg);
      renderlide(dataGlasses[removeArr]);
    } else {
      removeArr = 8;
      testGlasses(dataGlasses[removeArr].virtualImg);
      renderlide(dataGlasses[removeArr]);
    }
  }
};
window.removeGlasses = removeGlasses;
