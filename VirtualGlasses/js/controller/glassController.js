export let glassController = {
  renderHTML: (dataGlasses) => {
    let html = "";
    for (let i = 0; i < dataGlasses.length; i++) {
      html += ` <img class="col-4 py-3" src=" ${dataGlasses[i].src}" alt="" onclick="changeGlasses('${dataGlasses[i].id}')">`;
    }
    let divhtml = `<div class="row">${html}</div>`;
    document.getElementById("vglassesList").innerHTML = divhtml;
  },
};
